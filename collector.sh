#!/bin/bash

DOWNLOAD_TARGET=$1
DIR_NAME=$(basename $PWD)
TARGET_NAME=$(date +"$DIR_NAME-%Y-%m-%d-%H-%M-%S")

printf "Creating directory %s..." "$TARGET_NAME"
mkdir $TARGET_NAME
printf "done\n"

printf "Downloading %s..." "$DOWNLOAD_TARGET"
wget "$DOWNLOAD_TARGET" -P "$TARGET_NAME"
printf "done\n"
